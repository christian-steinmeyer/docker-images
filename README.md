# Docker Image Templates
In this project, my frequently used docker images are collected.
This repository does not include *ready to use* images,
but collects only Dockerfile templates.
They have project dependent requirements,
that prohibit me from hosting them centrally.


## Usage
You can use them as a starting point for your own images
or include them via GitLab's Continuous Integration.
If you choose the latter,
note, that this approach requires an access token in this repository.
Further it requires that token to be stored in a [variable] of the calling repository,
that executes the CI script.


### Use from Gitlab CI using Template
Alternatively, you can also use [my default template] in your CI script.
Simply include it like this:
```yaml
include:
  - project: 'christian-steinmeyer/ci-templates'
    file: '/jobs/.pre/.pull-Dockerfile.yml'

pull Dockerfile:
    extends:
        - .pull Dockerfile
    variables:
        PATH_TO_DOCKERFILE: "conda%2Finstall-environment-from-file"
```
In this example, you:
- include our template for pulling Dockerfiles from this repository
- define a job `pull Dockerfile` that extends our hidden job `pull Dockerfile` (note the dot)
- define the path to the dockerfile of interest

Note, that this approach requires the variable storing the access token to be called `DOCKER_IMAGE_TEMPLATES_ACCESS_TOKEN`.


### Use from custom Gitlab CI
You can use a specific dockerfile from this repository in your ci pipeline.
To that end, you could proceed as in this example:

```yaml
pull Dockerfile:
    variables:
        DOCKER_IMAGE_TEMPLATES_REPOSITORY: "christian-steinmeyer%2Fdocker-images"
        DOCKER_IMAGE_TEMPLATES_FILE_API: ${CI_API_V4_URL}/projects/${DOCKER_IMAGE_TEMPLATES_REPOSITORY}/repository/files/
        PATH_TO_DOCKERFILE: "conda%2Finstall-environment-from-file"
        POST_FIX: "/raw?ref=master"
        URL: "${DOCKER_IMAGE_TEMPLATES_FILE_API}${PATH_TO_DOCKERFILE}${POST_FIX}"
    stage: .pre
    script:
        - 'wget --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" $URL -O Dockerfile'
    artifacts:
        paths:
            - Dockerfile
        expire_in: 1 hour
```
In this example, the job `pull Dockerfile`:
- defines variables for
  - the repository to read from
  - the api base path to use for any file from that repository
  - the path to the file within the repository, but slashes are replaced with "%2F"
  - the postfix to use an exact copy of the file and use the latest version on the master branch
  - the final URL used in request
- assign the job to the .pre(pare) stage
- makes a request to the via the [Gitlab API] defined above and writes the output to a local file called "Dockerfile"
- upload the new file, in order for other jobs to have access to the file
- deletes the artifact after an hour (and assumes the pipeline duration is less)

Note, that this example requires the variable storing the access token to be called `PRIVATE_TOKEN`.

<!-- References -->
[Gitlab API]: https://docs.gitlab.com/ee/api/
[my default template]: https://gitlab.com/christian-steinmeyer/ci-templates/-/blob/master/jobs/.pre/.pull-Dockerfile.yml
[variable]: https://docs.gitlab.com/ee/security/cicd_environment_variables.html
