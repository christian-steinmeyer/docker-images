# Recursively finds all files without dots in their name
# and runs a docker linter (see https://github.com/hadolint/hadolint)

exit_code=0
find . -type f \( ! -iname "*.*" ! -path "./.*/*" \) | while read file
do
    hadolint "$file"
    hadolint_exit_code=$?
    if [[ "$hadolint_exit_code" -ne 0 ]]
    then
        echo "$file has the above errors" >&2
        exit_code=1
    fi
done
exit $exit_code

